<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps =  false;

    protected $fillable = ['title'];

    public function posts()
    {
        return $this->belongsToMany(Post::class,'category_post','category_id','post_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class,'parent_id');
//        return self::where('parent_id',$this->id);
    }

    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');
    }

}
